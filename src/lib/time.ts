import { PomodoroConstants } from '../constants/pomodoroConstants'

const CONSTANTS = PomodoroConstants

/**
 * Returns a string that will format time to MM:SS
 *
 * @param {number} seconds
 * @returns {string}
 *
 */

export const secToMMSS = (sec: number) => {
  return new Date(sec * CONSTANTS.MILISEC)
    .toISOString()
    .substr(CONSTANTS.START_ISO_STR, CONSTANTS.ISO_STR_LENGTH)
}
