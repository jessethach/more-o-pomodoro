import React, { Component } from 'react'
import './App.css'
import Navigation from './components/Header'
import Pomodoro from './components/Pomodoro'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faPlayCircle, faPauseCircle } from '@fortawesome/free-solid-svg-icons'
import { PomodoroConstants } from './constants/pomodoroConstants'

const CONSTANTS = PomodoroConstants

library.add(faPlayCircle, faPauseCircle)

interface State {
  breakTime: boolean
  duration: number
}

interface Props {}

class App extends Component<Props, State> {
  state = {
    breakTime: false,
    duration: CONSTANTS.DEFAULT_DURATION,
  }

  handleBreakTimeState = (breakTime: boolean, duration: number) => {
    this.setState({ breakTime, duration })
  }

  public render() {
    const { breakTime, duration } = this.state

    return (
      <div className="App">
        <Navigation breakTime={breakTime} duration={duration} />
        <Pomodoro onStateChange={this.handleBreakTimeState} />
      </div>
    )
  }
}

export default App
