export enum PomodoroConstants {
  // Durations in seconds
  DEFAULT_DURATION = 1500,
  DEFAULT_BREAK_DURATION = 300,
  // Increase break interval
  INCREASED_BREAK_INTERVAL = 900,
  // Milisecond for division purposes
  MILISEC = 1000,
  // ISO str variables
  START_ISO_STR = 14,
  ISO_STR_LENGTH = 5,
  // Increase break interval
  AMOUNT = 5,
}
