import 'bulma/css/bulma.css'
import React, { SFC, Fragment } from 'react'
import cn from 'classnames'
import { secToMMSS } from '../../lib/time'

interface Props {
  breakTime: boolean
  duration: number
}

const Navigation: SFC<Props> = ({ breakTime, duration }) => {
  const message = breakTime
    ? `Take a break for ${secToMMSS(duration)}!`
    : `On the grind for ${secToMMSS(duration)}!`

  return (
    <Fragment>
      <nav
        data-qa="navbar-comp"
        className={cn('navbar', {
          ['nav-color-break']: breakTime,
          ['nav-color-productivity']: !breakTime,
        })}
        role="navigation"
        aria-label="main navigation"
      >
        <div className="navbar-brand">
          <div className="navbar-item has-text-white">{message}</div>
        </div>
      </nav>
      <hr className="navbar-divider" />
    </Fragment>
  )
}

export default Navigation
