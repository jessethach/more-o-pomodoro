import React from 'react'
import { mount } from 'enzyme'
import Navigation from './index'
import { PomodoroConstants } from '../../constants/pomodoroConstants'

const CONSTANTS = PomodoroConstants

describe('Header component', () => {
  it('should render a Header component with productivity className', () => {
    const breakTime = false
    const duration = CONSTANTS.DEFAULT_DURATION
    const wrapper = mount(<Navigation breakTime={breakTime} duration={duration} />)

    expect(wrapper.exists()).toEqual(true)
    expect(wrapper.find('[data-qa="navbar-comp"]').prop('className')).toContain(
      'nav-color-productivity'
    )
  })

  it('should render a Header component with break className', () => {
    const duration = CONSTANTS.DEFAULT_DURATION
    const wrapper = mount(<Navigation breakTime duration={duration} />)

    expect(wrapper.exists()).toEqual(true)
    expect(wrapper.find('[data-qa="navbar-comp"]').prop('className')).toContain('nav-color-break')
  })
})
