import React, { Component, MouseEvent } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { PomodoroConstants } from '../../constants/pomodoroConstants'
import { secToMMSS } from '../../lib/time'

const CONSTANTS = PomodoroConstants

export interface State {
  duration: number
  displayDuration: string
  counter: number
  breakTime: boolean
  timeLineWidth: number
  // Current time in seconds
  currentTime: number | string
  // Current time in MMSS format
  displayTime: string
  showPlay: boolean
  onPlayHead: boolean
}

interface Props {
  onStateChange: (e: boolean, duration: number) => void
}

class Pomodoro extends Component<Props, State> {
  private timeLineEl: HTMLDivElement
  private playHeadEl: HTMLDivElement
  private timer: number

  state = {
    duration: CONSTANTS.DEFAULT_DURATION,
    displayDuration: secToMMSS(CONSTANTS.DEFAULT_DURATION),
    breakTime: false,
    currentTime: 0,
    displayTime: '00:00',
    counter: 0,
    timeLineWidth: 0,
    showPlay: true,
    onPlayHead: false,
  }

  setTimeLineRef = (timeLine: HTMLDivElement) => {
    this.timeLineEl = timeLine
  }

  setPlayHeadRef = (playHead: HTMLDivElement) => {
    this.playHeadEl = playHead
  }

  componentDidMount() {
    const timeLineWidth = this.timeLineEl.offsetWidth - this.playHeadEl.offsetWidth
    const displayDuration = secToMMSS(this.state.duration)
    this.setState({ timeLineWidth, displayDuration })
  }

  productivityCountDown = () => {
    const { currentTime, duration } = this.state
    const newCurrentTime = currentTime + 1
    this.setState({ currentTime: newCurrentTime })
    if (newCurrentTime >= duration) {
      this.recordCounter()
    }
    this.handleTimeUpdate()
  }

  recordCounter = () => {
    const counter = this.state.counter + 1
    this.setState({ breakTime: !this.state.breakTime, counter }, this.recordRounds)
  }

  recordRounds = () => {
    const { breakTime } = this.state
    if (breakTime) {
      if (this.state.counter % CONSTANTS.AMOUNT === 0) {
        this.setState(
          {
            duration: CONSTANTS.INCREASED_BREAK_INTERVAL,
            displayDuration: secToMMSS(CONSTANTS.INCREASED_BREAK_INTERVAL),
          },
          this.handleStateChange
        )
      } else {
        this.setState(
          {
            duration: CONSTANTS.DEFAULT_BREAK_DURATION,
            displayDuration: secToMMSS(CONSTANTS.DEFAULT_BREAK_DURATION),
          },
          this.handleStateChange
        )
      }
    } else {
      this.setState({ duration: CONSTANTS.DEFAULT_DURATION }, this.handleStateChange)
    }

    this.resetTimeLine()
    clearInterval(this.timer)
  }

  handleStateChange = () => {
    this.props.onStateChange(this.state.breakTime, this.state.duration)
  }

  resetTimeLine = () => {
    const { timeLineWidth } = this.state
    this.playHeadEl.style.marginLeft = `${timeLineWidth}px`
    this.setState({ showPlay: true, currentTime: 0, displayTime: '00:00' }, this.togglePlay)
  }

  togglePlay = () => {
    const { showPlay } = this.state
    if (showPlay) {
      this.timer = setInterval(this.productivityCountDown.bind(this), CONSTANTS.MILISEC)
      this.setState({ showPlay: false })
    } else {
      clearInterval(this.timer)
      this.setState({ showPlay: true })
    }
  }

  handleTimeUpdate = () => {
    const { timeLineWidth, currentTime, duration } = this.state
    const playPercent = timeLineWidth * (currentTime / duration)
    this.playHeadEl.style.marginLeft = `${playPercent}px`
    this.setState({ displayTime: secToMMSS(currentTime) })
  }

  movePlayHead = (e: MouseEvent<HTMLDivElement>) => {
    const { timeLineWidth } = this.state
    const newMargLeft = e.clientX - this.getTimeLinePosition()
    if (newMargLeft >= 0 && newMargLeft <= timeLineWidth) {
      this.playHeadEl.style.marginLeft = `${newMargLeft}px`
    }
    if (newMargLeft < 0) {
      this.playHeadEl.style.marginLeft = '0px'
    }
    if (newMargLeft > timeLineWidth) {
      this.playHeadEl.style.marginLeft = `${timeLineWidth}px`
    }
  }

  onMouseDownPlayHead = () => {
    this.setState({ onPlayHead: true })
  }

  onMouseUpPlayHead = (e: MouseEvent<HTMLDivElement>) => {
    if (this.state.onPlayHead) {
      const { duration } = this.state
      this.movePlayHead(e)
      this.setState({ currentTime: duration * this.clickPercent(e) })
      this.handleTimeUpdate()
    }
    this.setState({ onPlayHead: false })
  }

  onClickMovePlayHead = (e: MouseEvent<HTMLDivElement>) => {
    this.movePlayHead(e)
    const currentTime = this.state.duration * this.clickPercent(e)
    const displayTime = secToMMSS(currentTime)
    this.setState({ currentTime, displayTime })
  }

  clickPercent(e: MouseEvent<HTMLDivElement>) {
    return (e.clientX - this.getTimeLinePosition()) / this.state.timeLineWidth
  }

  getTimeLinePosition = () => {
    return this.timeLineEl.getBoundingClientRect().left
  }

  render() {
    const { displayTime, showPlay } = this.state
    const playPauseIcon = showPlay ? 'play-circle' : 'pause-circle'

    return (
      <section className="section">
        <div className="container">
          <h1>Pomodoro</h1>
          <h2>
            <p>{displayTime}</p>
          </h2>
        </div>
        <div className="timeline-container">
          <div className="play-pause-container">
            <button data-qa={`${playPauseIcon}-btn`} role="button" onClick={this.togglePlay}>
              <FontAwesomeIcon
                data-qa={playPauseIcon}
                className="icon"
                icon={playPauseIcon}
                size="2x"
                spin={playPauseIcon === 'pause-circle'}
              />
            </button>
          </div>
          <div
            data-qa="timeline"
            className="timeline"
            onClick={this.onClickMovePlayHead}
            ref={this.setTimeLineRef}
          >
            <div
              data-qa="play-head"
              role="button"
              className="play-head"
              onMouseDown={this.onMouseDownPlayHead}
              ref={this.setPlayHeadRef}
            />
          </div>
        </div>
      </section>
    )
  }
}

export default Pomodoro
