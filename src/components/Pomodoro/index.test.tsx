import React from 'react'
import { mount, ReactWrapper } from 'enzyme'
import Pomodoro, { State as PomodoroState } from './index'

describe('Pomodoro component', () => {
  let handleBreakTimeState: jest.Mock
  let wrapper: ReactWrapper

  beforeEach(() => {
    handleBreakTimeState = jest.fn()
    wrapper = mount(<Pomodoro onStateChange={handleBreakTimeState} />)
  })

  it('should render a Pomodoro component', () => {
    expect(wrapper.exists()).toEqual(true)
  })

  it('should show pause icon by default', () => {
    expect(wrapper.find('[data-qa="play-circle"]')).toHaveLength(1)
  })

  it('should change to play btn state when clicked', () => {
    expect((wrapper.state() as PomodoroState).showPlay).toEqual(true)
    wrapper.find('[data-qa="play-circle-btn"]').simulate('click')
    expect((wrapper.state() as PomodoroState).showPlay).toEqual(false)
  })
})
