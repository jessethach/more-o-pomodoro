### View app here:

[More-o-Pomodoro](https://more-o-pomodoro.herokuapp.com/)

### Getting Started

- Ensure you have [Node](https://nodejs.org/en/)
- Clone this repo

  ```
  $ git clone git clone https://jessethach@bitbucket.org/jessethach/more-o-pomodoro.git
  ```

  - Install dependencies

  ```
  $ npm install
  ```

  - Run the app locally

  ```
  $ npm start
  ```

  - Run tests

  ```
  $ npm test
  ```

##### Tech

- Enzyme for testing
- React/TypeScript
- Bulma.io for CSS layout

#### MVP

- ~~Timeline~~
- ~~Duration~~
- ~~Alert~~
- ~~Play/pause button~~

#### Stretch Goals

- Alerts with sound
  - Mute button
- ~~Color theming for work/break times~~
- Persist session into LocalStorage
- Add routes
- Form for user input
  - Form validation
